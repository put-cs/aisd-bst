#ifndef PUT_BST_H
#define PUT_BST_H
#include <stddef.h>

typedef struct node{
    int value;
    struct node *left;
    struct node *right;
}node;
// tree functions
node* create_node(int value);
node* array_to_balanced_bst(int arr[], int start, int end);
int insert_node(node** root_pointer, int value);
int bst_search(node* root, int target);
int bst_height(node *node);
void print_tree(node *root, int space);
void free_tree(node * node);
void balanced_arrangement(int size_to_sort,int *to_sort,int left, int right,int *tab);
void balanced_arrangement_function(int *to_sort,int size_to_sort);

//other functions
void printArray(int arr[], int size);
void shuffle_array(int *array, size_t n);

//sorting
int partition (int arr[], int low, int high);
void quickSort(int arr[], int low, int high);
void swap(int* a, int* b);

//searching
int binary_search(int array[], int left, int right, int target);
int linear_search(int array[], int n, int target);


#endif
