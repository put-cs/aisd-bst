#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "put_bst.h"
#define INDENT 4

node* create_node(int value){
    node* result = (node*)malloc(sizeof(node));
    if (result != NULL) {
        result->value = value;
        result->left = NULL;
        result->right = NULL;
    }
    return result;
}

int insert_node(node** root_pointer, int value){
    node* root = *root_pointer;

    if (root == NULL) {
        ///dotarlo do miejsca bez dalszych wartosci
        (*root_pointer) = create_node(value);
        return 1;
    }
    if (value == root->value)
        //nic nie rob - nie ma powtorzen w drzewie
        return 0;

    if (value < root->value)
        return insert_node(&(root->left), value);

    if (value > root->value)
        return insert_node(&(root->right), value);

    return -1;
}

int bst_search(node* root, int target){
    if (root == NULL) {
        return 0;
    }
    if (root->value == target) {
        // puts("Success! Found the target");
        return 1;
    }
    if (target <= root->value) {
        return bst_search(root->left, target);
    }
    if (target > root->value) {
        return bst_search(root->right, target);
    }
    return 0;
}


/*rekurencyjna funkcja szukania binarnego.
parametrami są:
1.tablica po której szukamy,
2.lewy skrajny indeks tablicy(przy 1. wywolaniu funkcji indeks 0)[pozniej wymagany w rekurencji],
3.prawy skrajny indeks tablicy(przy 1. wywolaniu indeks dlugosci tablicy - 1),
4.szukana wartosc*/
int binary_search(int array[], int left, int right, int target){
    if (right >= left){
        int mid = left + (right - left) / 2;

        //base case: szukany element jest elementem w srodku
        if (array[mid] == target){
            //printf("Target found!: target: %d\n", target);
            return mid;
        }

        //srodkowa wartosc mniejszy od szukanej wartosci,
        //wywolujemy funkcje dla lewej strony podtablicy
        if (array[mid] > target){
            return binary_search(array, left, mid - 1, target);
        }

        //srodkowa wartosc jest wiekszy od szukanej wartosci,
        //wywolujemy funkcje dla prawej strony podtablicy
        else if (array[mid] < target){
            return binary_search(array, mid + 1, right, target);
        }
    }

    //element nie został znaleziony
    //printf("Target not found.: %d\n", target);
    return -1;
}

void print_tree(node *root, int space){
    if (root == NULL)
        return;
    space += INDENT;
    print_tree(root->right, space);
    for (int i = INDENT; i < space; i++)
        printf(" ");
    printf("%d\n", root->value);
    print_tree(root->left, space);
}


void swap(int* a, int* b){
    int t = *a;
    *a = *b;
    *b = t;
}

int partition (int array[], int low, int high){
    int pivot = array[high];
    int i = (low - 1);
    for (int j = low; j <= high- 1; j++){
        if (array[j] <= pivot){
            i++;
            swap(&array[i], &array[j]);
        }
    }
    swap(&array[i + 1], &array[high]);
    return (i + 1);
}

void quickSort(int array[], int low, int high){
    if (low < high){
        int pivot = partition(array, low, high);
        quickSort(array, low, pivot - 1);
        quickSort(array, pivot + 1, high);
    }
}

void printArray(int array[], int size){
    int i;
    for (i=0; i < size; i++)
        printf("%d ", array[i]);
}

int generate_random(int left, int right) { //this will generate random number in range l and r
    srand(time(0));
    int rand_num = (rand() % (right - left + 1)) + left;
    return rand_num;
}

void shuffle_array(int *array, size_t n){
    if (n > 1){
        size_t i;
        for (i = 0; i < n - 1; i++){
            size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
            swap(&array[i], &array[j]);
        }
    }
}

int linear_search(int *array, int n, int target){
    for (int i=0; i<n; i++) {
        if (array[i] == target) {
            //printf("found %d at index %d\n", target, i);
            return EXIT_SUCCESS;
        }
    }
    //printf("not found %d\n", target);
    return EXIT_FAILURE;
}

//funkcja szukajaca wysokosc drzewa
int bst_height(node *node){
    //base case
    if(node == NULL){
        return 0;
    }
    else{
        int left_side, right_side;
        left_side = bst_height(node -> left);
        right_side = bst_height(node -> right);
        if(left_side > right_side){
            return left_side + 1;
        }
        else{
            return right_side + 1;
        }
    }
}

int spot;

void balanced_arrangement(int size_to_sort,int *to_sort,int left, int right,int *tab){
  if (right-left==0) {tab[spot]=to_sort[(right+left)/2];spot++; return;}
  if (right-left==1) {tab[spot]=to_sort[(right+left)/2];spot++; tab[spot]=to_sort[1+(right+left)/2];spot++; return;}
  tab[spot]=to_sort[(right+left)/2];
  spot++;
  balanced_arrangement(size_to_sort,to_sort,left, -1+(right+left)/2,tab);
  balanced_arrangement(size_to_sort,to_sort,1+(right+left)/2, right,tab);
}

void balanced_arrangement_function(int *to_sort,int size_to_sort){
    int tab[size_to_sort];
    int spot=0;
    balanced_arrangement(size_to_sort,to_sort,0,size_to_sort-1,tab);
}

// int* balanced_arrangement_function(int *to_sort,int size_to_sort){
//     int *tab = malloc(size_to_sort * sizeof(int));
//     int spot=0;
//     balanced_arrangement(size_to_sort,to_sort,0,size_to_sort-1,tab);
//     return (int*)tab;
// }

void free_tree(node * node){
        //post-order like FatalError hinted at
            if (node != NULL) {
            free_tree(node->right);
            // free(node->value); //if data was heap allocated, need to free it
            free_tree(node->left);
            free(node);
            }
        }

node* array_to_balanced_bst(int arr[], int start, int end)
{
    if (start > end)
      return NULL;

    int mid = (start + end)/2;
    node *root = create_node(arr[mid]);

    root->left =  array_to_balanced_bst(arr, start, mid-1);
    root->right = array_to_balanced_bst(arr, mid+1, end);

    return root;
}
