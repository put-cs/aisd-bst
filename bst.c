#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "put_bst.h"

int main(void){
    system("mkdir results > /dev/null");
    system("cd results && rm Cb.txt Sa.txt Sb.txt CTA.txt STA.txt HTA.txt CTB.txt STB.txt HTB.txt && cd .. > /dev/null");

    // node *root = NULL;
    // insert_node(&root, 11);

    // print_tree(root, 0);
    // printf("%d\n\n", bst_search(root, 15));

    FILE *Cbptr, *Saptr, *Sbptr, *CTAptr, *STAptr,*HTAptr, *CTBptr, *STBptr, *HTBptr, *test;

    srand(time(0));
    clock_t Cb, Sa, Sb, CTA, STA, CTB, STB;
    //Cb - kopiowanie i sortowanie w B
    //Sa - linear search w A
    //Sb - binary search w B
    //CTA - czas tworzenia drzewa TA (niebalansowane)
    //STA - czas wyszukiwania kolejnych el tablicy A w drzewie TA
    //HTA - wysokosc drzewa TA
    //CTB - czas tworzenia drzewa TB (zbalansowane)
    //STB - czas wyszukiwania kolejnych el tablicy A w drzewie TB
    //HTB - wysokosc drzewa TB

    int sizes[] = {1000, 2500, 5000, 7500, 10000, 12500, 15000, 20000, 30000, 50000, 70000, 100000};
    int num_of_tests; num_of_tests = sizeof(sizes)/sizeof(int);
    int array_size;
    for (int i=0; i < num_of_tests; i++){
        ////////////////////////   TASK 1    ////////////////////////
        array_size = sizes[i];
        // printf("%d\n", array_size);
        int A[array_size];
        int B[array_size];

        //initialize A with numbers
        for (int i=0; i<array_size; i++){
            A[i] = i+1; // +1 bo liczby maja byc dodatnie
        }
        // puts("\n\n");

        /////////////////////////   TASK 2  /////////////////////////

        // shuffle the array for the later sorting to make sense
        shuffle_array(A, array_size);

        Cb = clock();
        for (int i=0; i<array_size;i++) {//copy array A to B
            B[i] = A[i];
        }
        quickSort(B, 0, array_size-1); //sort B
        Cb = clock() - Cb;

        Cbptr = fopen("results/Cb.txt","a");
        fprintf(Cbptr,"%d %f\n", array_size, (float)Cb/CLOCKS_PER_SEC);
        // printf("Czas tworzenia kopii i sortowania: %f\n", (float)Cb/CLOCKS_PER_SEC);
        fclose(Cbptr);

        int target;
        Sa = clock();
        for (int i=0; i<array_size; i++) {
            target = B[i];
            linear_search(A, array_size, target);
        }
        Sa = clock() - Sa;

        Saptr = fopen("results/Sa.txt", "a");
        fprintf(Saptr,"%d %f\n", array_size, (float)Sa/CLOCKS_PER_SEC);
        // printf("Czas linear search wartości z B w A: %f\n", (float)Sa/CLOCKS_PER_SEC);
        fclose(Saptr);

        Sb = clock();
        for (int i=0; i<array_size; i++) {
            target = A[i]; // bo w A są liczby od 1
            binary_search(B, 0, array_size-1, target);
        }
        Sb = clock() - Sb;
        Sbptr = fopen("results/Sb.txt", "a");
        fprintf(Sbptr,"%d %f\n", array_size, (float)Sb/CLOCKS_PER_SEC);
        // printf("Czas binary search wartości z A w B: %f\n", (float)Sb/CLOCKS_PER_SEC);
        fclose(Sbptr);

        /////////////////////////   TASK 3  /////////////////////////

        node* TA = NULL;

        CTA = clock();

        for (int i=0; i<array_size; i++) {
            insert_node(&TA, A[i]);
        }

        CTA = clock() - CTA;

        CTAptr = fopen("results/CTA.txt", "a");
        fprintf(CTAptr,"%d %f\n", array_size,(float)CTA/CLOCKS_PER_SEC);
        // printf("Czas tworzenia zwyklego drzewa binarnego %f\n", (float)CTA/CLOCKS_PER_SEC);
        fclose(CTAptr);

        STA = clock();
        for (int i=0; i < array_size; i++){
            bst_search(TA ,A[i]);
        }
        STA = clock() - STA;

        STAptr = fopen("results/STA.txt", "a");
        fprintf(STAptr,"%d %f\n", array_size, (float)STA / CLOCKS_PER_SEC);
        // printf("Czas wyszukiwania calej tablicy A w zwyklym drzewie binarnym %f\n", (float)STA / CLOCKS_PER_SEC);
        fclose(STAptr);

        int height = bst_height(TA);
        HTAptr = fopen("results/HTA.txt", "a");
        fprintf(HTAptr,"%d %d\n", array_size, height);
        // printf("Tree height: %d\n", height);
        fclose(HTAptr);
        free_tree(TA);

        /////////////////////////   TASK 4  /////////////////////////
        node* TB = NULL;
        int to_sort[array_size];
        for (int i=0; i < array_size; i++){
            to_sort[i] = i + 1;
        }
        shuffle_array(to_sort, array_size);

        CTB = clock();
        TB = array_to_balanced_bst(to_sort, 0, array_size);
        CTB = clock() - CTB;

        CTBptr = fopen("results/CTB.txt", "a");
        fprintf(CTBptr,"%d %f\n", array_size, (float)CTB/CLOCKS_PER_SEC);
        // printf("Czas tworzenia zbalansowanego drzewa: %f\n", (float)CTB/CLOCKS_PER_SEC);
        fclose(CTBptr);

        STB = clock();
        for (int i=0; i < array_size; i++){
            bst_search(TB, A[i]);
        }
        STB = clock() - STB;

        STBptr = fopen("results/STB.txt", "a");
        fprintf(STBptr,"%d %f\n",array_size, (float)STB / CLOCKS_PER_SEC);
        // printf("Czas wyszukiwania calej tablicy A w zbalansowanym drzewie binarnym %f\n", (float)STB / CLOCKS_PER_SEC);
        fclose(STBptr);

        int balanced_height = bst_height(TB);
        HTBptr = fopen("results/HTB.txt", "a");
        fprintf(HTBptr,"%d %d\n", array_size, balanced_height);
        // printf("Balanced tree height: %d\n\n", balanced_height);
        fclose(HTBptr);


        // int copy[array_size];
        // for (int i=0; i < array_size; i++){
        //     copy[i] = to_sort
        // }
        // balanced_arrangement_function(to_sort, array_size);
        // int *copy = malloc(array_size * sizeof(int));
        // copy = balanced_arrangement_function(to_sort, array_size);


        // for (int i=0; i < array_size; i++){
        //     // printf("%d\n",copy[i]);
        //     insert_node(&TB, to_sort[i]);
        // }
        // // printf("\n");
        // // print_tree(TB, 0);
        // // printf("\n");
        free_tree(TB);
    }
    return EXIT_SUCCESS;
}
